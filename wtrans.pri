INCLUDEPATH += \
    ../core/sources \

QT += \
    core \

QT -= \
    gui

CONFIG += \
    c++11 \
    console \
    debug_and_release \

CONFIG -= \
    app_bundle \

QMAKE_CXXFLAGS += \
    -Wall \
    -Wextra \
    -Werror \
    -pedantic \
    -pedantic-errors \
