TEMPLATE = subdirs

CONFIG += \
    ordered \
    debug_and_release \

SUBDIRS = \
    core \
    app \
    tests \

app.depends = core
tests.depends = core
