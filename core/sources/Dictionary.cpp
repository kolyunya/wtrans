#include <Dictionary.hpp>
#include <QtCore/QException>

Dictionary::Dictionary(QObject* parent):
    QObject(parent)
{
    Q_ASSERT(nullptr != parent);
    if (nullptr == parent) {
        throw QException();
    }
}

Word Dictionary::getWord(const QString& wordString) const
{
    Word word = nullptr;
    Words allWords = this->getAllWords();
    for (Word currentWord: allWords) {
        Q_ASSERT(nullptr != currentWord);
        if (nullptr == currentWord) {
            continue;
        }
        QString currentWordString = *currentWord;
        bool equalsWordString = currentWordString == wordString;
        if (equalsWordString) {
            word = currentWord;
            break;
        }
    }
    return word;
}

bool Dictionary::hasWord(const QString& wordString) const
{
    bool hasWord = false;

    Word word = this->getWord(wordString);
    if (nullptr == word) {
        return hasWord;
    }

    Words allWords = this->getAllWords();
    hasWord = allWords.contains(word);

    return hasWord;
}
