#ifndef TASK_HPP
#define TASK_HPP

#include <QObject>
#include <Dictionary.hpp>

class Task:
    public QObject
{
    public:
        explicit Task(QObject *parent);
        virtual ~Task() = default;
        virtual QString getSourceWord() const = 0;
        virtual QString getDestinationWord() const = 0;
};

#endif // TASK_HPP
