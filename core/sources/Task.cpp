#include <QtCore/QException>
#include <Task.hpp>

Task::Task(QObject* parent):
    QObject(parent)
{
    Q_ASSERT(nullptr != parent);
    if (nullptr == parent) {
        throw QException();
    }
}

