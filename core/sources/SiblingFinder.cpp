#include <QtCore/QException>
#include <SiblingFinder.hpp>

SiblingFinder::SiblingFinder(QObject* parent, const Dictionary* dictionary):
    QObject(parent),
    dictionary(dictionary)
{
    Q_ASSERT(nullptr != parent);
    if (nullptr == parent) {
        throw QException();
    }

    Q_ASSERT(nullptr != dictionary);
    if (nullptr == dictionary) {
        throw QException();
    }
}

Words SiblingFinder::getAllSiblings(const Word& word) const
{
    Words allSiblings;

    Q_ASSERT(nullptr != word);
    if (nullptr == word) {
        return allSiblings;
    }

    Words allWords = this->dictionary->getAllWords();
    for (const QString* currentWord: allWords) {
        Q_ASSERT(nullptr != currentWord);
        if (nullptr == currentWord) {
            continue;
        }
        bool areSiblings = this->areSiblings(word, currentWord);
        if (areSiblings) {
            allSiblings.append(currentWord);
        }
    }

    return allSiblings;
}
