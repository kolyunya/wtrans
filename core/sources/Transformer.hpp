#ifndef TRANSFORMER_HPP
#define TRANSFORMER_HPP

#include <Dictionary.hpp>
#include <QtCore/QObject>
#include <SiblingFinder.hpp>

class Transformer:
    public QObject
{
    public:
        explicit Transformer(QObject* parent,
                             const Dictionary* dictionary,
                             const SiblingFinder* siblingFinder);
        virtual Words transform(const Word& from, const Word& to) const = 0;
    protected:
        const Dictionary* dictionary;
        const SiblingFinder* siblingFinder;
};

#endif // TRANSFORMER_HPP
