#ifndef BGL_TRANSFORMER_HPP
#define BGL_TRANSFORMER_HPP

#include <Transformer.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/visitors.hpp>

struct VertexData { Word word; };
using Graph = boost::adjacency_list<boost::vecS,
                                    boost::vecS,
                                    boost::undirectedS,
                                    VertexData>;
using Vertex = Graph::vertex_descriptor;
using Edge = Graph::edge_descriptor;
using VertexIterator = Graph::vertex_iterator;
using VerticesPair = std::pair<VertexIterator, VertexIterator>;
using Vertices = std::list<VertexIterator>;

// This BFS visitor aborts BFS execution when the destination vertext was reached.
// Due to the BGL design we have thow exception in case of success to
// abort BFS execution when destination vertex reached.
// Yup... That's just ridiculous...
class BfsAborter:
    public boost::default_bfs_visitor
{
    public:
        using event_filter = boost::on_discover_vertex;
        BfsAborter(const Vertex& destination):
            destination(destination)
        {

        }
        void operator()(Vertex vertex, const Graph&) const
        {
            if (vertex == destination) {
                throw std::exception();
            }
        }
    private:
        const Vertex& destination;
};

class BglTransformer:
    public Transformer
{
    public:
        explicit BglTransformer(QObject* parent,
                                const Dictionary* dictionary,
                                const SiblingFinder* siblingFinder);
        Words transform(const Word& from, const Word& to) const override;
    private:
        void initialize();
        void constructGraph();
        void addVertex(const Word& word);
        bool hasVertex(const Word& word) const;
        bool hasEdge(const Word& firstVertex, const Word& secondVertex) const;
        VertexIterator getVertex(const Word& word) const;
        Graph graph;
};

#endif // BGL_TRANSFORMER_HPP
