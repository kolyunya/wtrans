#ifndef DICTIONARY_HPP
#define DICTIONARY_HPP

#include <QtCore/QList>
#include <QtCore/QObject>

using Word = const QString*;
using Words = QList<Word>;

class Dictionary:
    public QObject
{
    public:
        explicit Dictionary(QObject* parent);
        virtual ~Dictionary() = default;
        virtual Words getAllWords() const = 0;
        Word getWord(const QString& wordString) const;
        bool hasWord(const QString& wordString) const;
};

#endif // DICTIONARY_HPP
