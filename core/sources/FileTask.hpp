#ifndef FILE_TASK_HPP
#define FILE_TASK_HPP

#include <Task.hpp>

class FileTask:
    public Task
{
    public:
        explicit FileTask(QObject* parent);
        QString getSourceWord() const override;
        QString getDestinationWord() const override;
    private:
        void loadWords();
        QString sourceWord;
        QString destinationWord;
        static const QString FILE_NAME;
};

#endif // FILE_TASK_HPP
