#ifndef FILE_DICTIONARY_HPP
#define FILE_DICTIONARY_HPP

#include <Dictionary.hpp>
#include <QtCore/QList>
#include <QtCore/QTextStream>

class FileDictionary : public Dictionary
{
    public:
        FileDictionary(QObject* parent);
        ~FileDictionary();
        Words getAllWords() const override;
    private:
        void loadDictionary();
        void loadWord(QTextStream& stream);
        QList<const QString*> words;
        static const QString FILE_NAME;
};

#endif // FILE_DICTIONARY_HPP
