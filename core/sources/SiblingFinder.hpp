#ifndef SIBLING_FINDER_HPP
#define SIBLING_FINDER_HPP

#include <Dictionary.hpp>

class SiblingFinder:
    public QObject
{
    public:
        explicit SiblingFinder(QObject* parent, const Dictionary* dictionary);
        virtual ~SiblingFinder() = default;
        virtual bool areSiblings(const Word& foo, const Word& bar) const = 0;
        Words getAllSiblings(const Word& word) const;
    private:
        const Dictionary* dictionary;
};

#endif // SIBLING_FINDER_HPP
