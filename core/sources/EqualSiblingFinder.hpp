#ifndef EQUAL_SIBLING_FINDER_HPP
#define EQUAL_SIBLING_FINDER_HPP

#include <SiblingFinder.hpp>

class EqualSiblingFinder:
    public SiblingFinder
{
    public:
        explicit EqualSiblingFinder(QObject* parent, Dictionary* dictionary);
        bool areSiblings(const Word& foo, const Word& bar) const override;
    private:
        static bool haveEqualLength(const Word& foo, const Word& bar);
        static bool differInOneLetter(const Word& foo, const Word& bar);
};

#endif // EQUAL_SIBLING_FINDER_HPP
