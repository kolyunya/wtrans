#include <FileTask.hpp>
#include <QtCore/QFile>
#include <QtCore/QTextStream>

const QString FileTask::FILE_NAME = "task";

FileTask::FileTask(QObject* parent):
    Task(parent)
{
    this->loadWords();
}

QString FileTask::getSourceWord() const
{
    return this->sourceWord;
}

QString FileTask::getDestinationWord() const
{
    return this->destinationWord;
}

void FileTask::loadWords()
{
    QFile file(FileTask::FILE_NAME);
    bool fileWasOpened = file.open(QIODevice::ReadOnly);
    if (!fileWasOpened) {
        return;
    }

    QTextStream fileStream(&file);
    this->sourceWord = fileStream.readLine();
    this->destinationWord = fileStream.readLine();
}
