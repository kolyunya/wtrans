#include <boost/graph/lookup_edge.hpp>
#include <BglTransformer.hpp>

BglTransformer::BglTransformer(QObject* parent,
                                   const Dictionary* dictionary,
                                   const SiblingFinder* siblingFinder):
    Transformer(parent, dictionary, siblingFinder)
{
    this->initialize();
}

Words BglTransformer::transform(const Word& from, const Word& to) const
{
    Words transformation;

    Q_ASSERT(nullptr != from);
    if (nullptr == from) {
        return transformation;
    }

    Q_ASSERT(nullptr != to);
    if (nullptr == to) {
        return transformation;
    }

    Vertex fromVertex = *this->getVertex(from);
    Vertex toVertex = *this->getVertex(to);

    // This visitor will abort algorithm execution when `toVertex` will be reached
    BfsAborter bfsAborter(fromVertex);

    // This visitor will record each vertex predecessors to backtrace a path from
    // `fromVertex` to `toVertex`
    std::vector<Vertex> predecessors(boost::num_vertices(this->graph));
    predecessors[toVertex] = toVertex;

    try
    {
        boost::breadth_first_search(
            this->graph,
            toVertex,
            boost::visitor(
                boost::make_bfs_visitor(
                    std::make_pair(
                        boost::record_predecessors(
                            &predecessors[0],
                            boost::on_tree_edge()
                        ),
                        bfsAborter
                    )
                )
            )
        );
        // Destination vertex wasn not reached if exception was not thrown
    }
    catch (std::exception) {
        // Destination vertex was reached if exception was thrown
        Vertex currentVertex = fromVertex;
        while (currentVertex != toVertex) {
            Word currentWord = this->graph[currentVertex].word;
            transformation.append(currentWord);
            currentVertex = predecessors[currentVertex];
        }
        transformation.append(this->graph[toVertex].word);
    }

    return transformation;
}

void BglTransformer::initialize()
{
    this->constructGraph();
}

void BglTransformer::constructGraph()
{
    const Words allWords = this->dictionary->getAllWords();
    for (const Word& word: allWords) {
        Q_ASSERT(nullptr != word);
        if (nullptr == word) {
            continue;
        }
        this->addVertex(word);
    }
}

void BglTransformer::addVertex(const Word& word)
{
    Q_ASSERT(nullptr != word);
    if (nullptr == word) {
        return;
    }

    Vertex wordVertex = *(this->getVertex(word));
    if (!this->hasVertex(word)) {
        wordVertex = boost::add_vertex(this->graph);
        this->graph[wordVertex].word = word;
    }

    const Words allSiblings = this->siblingFinder->getAllSiblings(word);
    for (const Word& sibling: allSiblings) {
        Q_ASSERT(nullptr != sibling);
        if (nullptr == sibling) {
            continue;
        }
        Vertex siblingVertex = *(this->getVertex(sibling));
        if (!this->hasVertex(sibling)) {
            siblingVertex = boost::add_vertex(this->graph);
            this->graph[siblingVertex].word = sibling;
        }
        if (!this->hasEdge(word, sibling)) {
            boost::add_edge(wordVertex, siblingVertex, this->graph);
        }
    }
}

bool BglTransformer::hasVertex(const Word& word) const
{
    VertexIterator vertexIterator = this->getVertex(word);
    VertexIterator verticesEnd = boost::vertices(this->graph).second;
    bool hasVertex = vertexIterator != verticesEnd;
    return hasVertex;
}

bool BglTransformer::hasEdge(const Word& firstVertex, const Word& secondVertex) const
{
    auto edgeInfo = boost::lookup_edge(
        *this->getVertex(firstVertex),
        *this->getVertex(secondVertex),
        this->graph
    );
    bool hasEdge = edgeInfo.second;
    return hasEdge;
}

VertexIterator BglTransformer::getVertex(const Word& word) const
{
    VerticesPair verticesRange = boost::vertices(this->graph);
    VertexIterator destinationVertexIterator = verticesRange.second;
    VertexIterator currentVertexIterator = verticesRange.first;
    while (currentVertexIterator != verticesRange.second) {
        Vertex currentVertex = *currentVertexIterator;
        Word currentVertexWord = this->graph[currentVertex].word;
        if (currentVertexWord == word) {
            destinationVertexIterator = currentVertexIterator;
            break;
        }
        currentVertexIterator++;
    }
    return destinationVertexIterator;
}
