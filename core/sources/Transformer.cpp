#include <QtCore/QException>
#include <Transformer.hpp>

Transformer::Transformer(QObject* parent,
                         const Dictionary* dictionary,
                         const SiblingFinder* siblingFinder):
    QObject(parent),
    dictionary(dictionary),
    siblingFinder(siblingFinder)
{
    Q_ASSERT(nullptr != parent);
    if (nullptr == parent) {
        throw QException();
    }

    Q_ASSERT(nullptr != dictionary);
    if (nullptr == dictionary) {
        throw QException();
    }

    Q_ASSERT(nullptr != siblingFinder);
    if (nullptr == siblingFinder) {
        throw QException();
    }
}
