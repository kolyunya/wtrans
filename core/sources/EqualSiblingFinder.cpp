#include <EqualSiblingFinder.hpp>

EqualSiblingFinder::EqualSiblingFinder(QObject* parent, Dictionary* dictionary):
    SiblingFinder(parent, dictionary)
{

}

bool EqualSiblingFinder::areSiblings(const Word& foo, const Word& bar) const
{
    bool areSiblings = false;

    if (nullptr == foo) {
        return areSiblings;
    }

    if (nullptr == bar) {
        return areSiblings;
    }

    areSiblings = (
        EqualSiblingFinder::haveEqualLength(foo, bar) &&
        EqualSiblingFinder::differInOneLetter(foo, bar)
    );

    return areSiblings;
}

bool EqualSiblingFinder::haveEqualLength(const Word& foo, const Word& bar)
{
    bool haveEqualLength = foo->length() == bar->length();
    return haveEqualLength;
}

bool EqualSiblingFinder::differInOneLetter(const Word& foo, const Word& bar)
{
    int differentLettersCount = 0;
    int letterIndex = 0;
    while (letterIndex < foo->length()) {
        QChar fooChar = foo->at(letterIndex);
        QChar barChar = bar->at(letterIndex);
        bool differentChars = fooChar != barChar;
        if (differentChars) {
            differentLettersCount++;
        }
        if (differentLettersCount > 1) {
            break;
        }
        letterIndex++;
    }
    bool differInOneLetter = differentLettersCount == 1;
    return differInOneLetter;
}
