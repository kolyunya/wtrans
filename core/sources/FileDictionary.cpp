#include <FileDictionary.hpp>
#include <QtCore/QFile>

const QString FileDictionary::FILE_NAME = "dictionary";

FileDictionary::FileDictionary(QObject* parent):
    Dictionary(parent)
{
    this->loadDictionary();
}

FileDictionary::~FileDictionary()
{
    for (const Word& word: this->words) {
        delete word;
    }
}

Words FileDictionary::getAllWords() const
{
    return this->words;
}

void FileDictionary::loadDictionary()
{
    QFile file(FileDictionary::FILE_NAME);
    bool fileWasOpened = file.open(QIODevice::ReadOnly);
    if (!fileWasOpened) {
        return;
    }

    QTextStream fileStream(&file);
    while (!fileStream.atEnd()) {
        this->loadWord(fileStream);
    }

    file.close();
}

void FileDictionary::loadWord(QTextStream& stream)
{
    QString wordData = stream.readLine();
    this->words.append(new QString(wordData));
}
