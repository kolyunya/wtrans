include(../wtrans.pri)

TARGET = wtrans-core

TEMPLATE = lib

INCLUDEPATH += \
    dependencies \
    sources \

HEADERS += \
    sources/BglTransformer.hpp \
    sources/Dictionary.hpp \
    sources/EqualSiblingFinder.hpp \
    sources/FileDictionary.hpp \
    sources/FileTask.hpp \
    sources/SiblingFinder.hpp \
    sources/Task.hpp \
    sources/Transformer.hpp \

SOURCES += \
    sources/BglTransformer.cpp \
    sources/Dictionary.cpp \
    sources/EqualSiblingFinder.cpp \
    sources/FileDictionary.cpp \
    sources/FileTask.cpp \
    sources/SiblingFinder.cpp \
    sources/Task.cpp \
    sources/Transformer.cpp \
