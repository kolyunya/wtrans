#include <EqualSiblingFinder.hpp>
#include <EqualSiblingFinderTests.hpp>
#include <FileDictionary.hpp>
#include <QtTest/QTest>

void EqualSiblingFinderTests::initTestCase()
{
    this->dictionary = new FileDictionary(this);
    this->siblingFinder = new EqualSiblingFinder(this, this->dictionary);
}

void EqualSiblingFinderTests::areSiblings_001()
{
    Word wordFoo = new QString("foo");
    Word wordBar = new QString("bar");
    bool areSiblings = this->siblingFinder->areSiblings(wordFoo, wordBar);

    // Those words are not siblings since they differ more than in one letter
    QVERIFY(areSiblings == false);
}

void EqualSiblingFinderTests::areSiblings_002()
{
    Word wordFoo = new QString("foo");
    Word wordBar = new QString("quux");
    bool areSiblings = this->siblingFinder->areSiblings(wordFoo, wordBar);

    // Those words are not siblings since they differ have different length
    QVERIFY(areSiblings == false);
}

void EqualSiblingFinderTests::areSiblings_003()
{
    Word wordFoo = new QString("foo");
    Word wordBar = new QString("foo");
    bool areSiblings = this->siblingFinder->areSiblings(wordFoo, wordBar);

    // Those words are not siblings since they differ less then in one letter
    QVERIFY(areSiblings == false);
}

void EqualSiblingFinderTests::areSiblings_004()
{
    Word wordFoo = new QString("aaa");
    Word wordBar = new QString("aab");
    bool areSiblings = this->siblingFinder->areSiblings(wordFoo, wordBar);

    // Those words are siblings since they differ in exactly one letter
    // and have equal length
    QVERIFY(areSiblings == true);
}

void EqualSiblingFinderTests::areSiblings_005()
{
    Word wordFoo = new QString("foo");
    Word wordBar = nullptr;
    bool areSiblings = this->siblingFinder->areSiblings(wordFoo, wordBar);

    // Those words are not siblings since one word is missing
    QVERIFY(areSiblings == false);
}

void EqualSiblingFinderTests::areSiblings_006()
{
    Word wordFoo = new QString("a");
    Word wordBar = new QString("");
    bool areSiblings = this->siblingFinder->areSiblings(wordFoo, wordBar);

    // Those words are not siblings since one word is empty
    QVERIFY(areSiblings == false);
}

void EqualSiblingFinderTests::cleanupTestCase()
{

}

