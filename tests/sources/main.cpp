#include <EqualSiblingFinderTests.hpp>
#include <QtTest/QTest>

int main()
{
    EqualSiblingFinderTests equalSiblingFinderTests;
    QTest::qExec(&equalSiblingFinderTests);

    return 0;
}
