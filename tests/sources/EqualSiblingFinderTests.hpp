#ifndef EQUALSIBLINGFINDERTESTS_HPP
#define EQUALSIBLINGFINDERTESTS_HPP

#include <QtCore/QObject>
#include <SiblingFinder.hpp>

class EqualSiblingFinderTests:
    public QObject
{
    Q_OBJECT
    private:
        Dictionary* dictionary;
        SiblingFinder* siblingFinder;
    private slots:
        void initTestCase();
        void areSiblings_001();
        void areSiblings_002();
        void areSiblings_003();
        void areSiblings_004();
        void areSiblings_005();
        void areSiblings_006();
        void cleanupTestCase();
};

#endif // EQUALSIBLINGFINDERTESTS_HPP
