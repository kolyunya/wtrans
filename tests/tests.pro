include(../wtrans.pri)

TEMPLATE = app

QT += \
    testlib \

CONFIG += \
    testcase \

INCLUDEPATH += \
    ../core/sources \
    ../core/dependencies \
    sources \

HEADERS += \
    sources/EqualSiblingFinderTests.hpp

SOURCES += \
    sources/main.cpp \
    sources/EqualSiblingFinderTests.cpp

LIBS += \
    -L../core \
    -L../core/debug \
    -L../core/release \
    -lwtrans-core \

QMAKE_RPATHDIR += \
    ../core \
    ../core/debug \
    ../core/release \
