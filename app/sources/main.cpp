#include <BglTransformer.hpp>
#include <EqualSiblingFinder.hpp>
#include <FileDictionary.hpp>
#include <FileTask.hpp>
#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>

int main(int argc, char** argv)
{
    QCoreApplication application(argc, argv);

    // Task which is loaded from a local file
    Task* task = new FileTask(&application);

    // Dictionary which is loaded from a local file
    Dictionary* dictionary = new FileDictionary(&application);

    // Sibling finder which finds siblings of equal length
    SiblingFinder* siblingFinder = new EqualSiblingFinder(&application, dictionary);

    // Word transformer implemented with boost graph library
    Transformer* transformer = new BglTransformer(&application, dictionary, siblingFinder);

    // Retrieve words from dictionary
    Word initialWord = dictionary->getWord(task->getSourceWord());
    Word finalWord = dictionary->getWord(task->getDestinationWord());

    // Calculate and print word transformation
    Words transformation = transformer->transform(initialWord, finalWord);
    for (const QString* word: transformation) {
        qDebug("%s", word->toStdString().c_str());
    }

    return 0;
}
