include(../wtrans.pri)

TEMPLATE = app

LIBS += \
    -L../core \
    -L../core/debug \
    -L../core/release \
    -lwtrans-core \

QMAKE_RPATHDIR += \
    ../core \
    ../core/debug \
    ../core/release \

SOURCES += \
    sources/main.cpp
